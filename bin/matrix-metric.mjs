#!/bin/env node

/* matrix_metric.mjs:

   Reports some metrics for a Matrix chat's exported JSON file.

   Usage:
   ./matrix_metric.js matrix.json

   (Right now, it's just expecting the filename matrix.json, but will make that a CLI arg soon)

   Author: Richard Schwarting <aquarichy@gmail.com> ©2023
   License: GPLv3+
 */

import * as fs   from "node:fs";
import * as os   from "node:os";
import * as path from "node:path";

function matrix_metric_words_of_interest (text_freq, text_iword_freq, sender_vocab_freq) {
  console.log ("\n\nWords of interest: ", [ ... words_of_interest ].join (", "));

  console.log ("\n# of words of interest used by user:");
  freq_show (text_iword_freq);

  console.log(`\nProbability that a message contains a word of interest by user (1.0 == 100%, 0.01 == 1%):`);
  for (const sender in text_iword_freq) {
    print_key_value_pair(sender, calc_ratio(text_iword_freq[sender] ?? 0, text_freq[sender], 4));
  }

  for (const word of words_of_interest) {
    console.log (`\nFrequency of '${word}' by user:`);
    for (const sender in sender_vocab_freq) {
      print_key_value_pair (sender, sender_vocab_freq[sender][word] ?? 0);
    }

    console.log (`\nProbability that a message contains '${word}' by user (1.0 == 100%, 0.01 == 1%):`);
    for (const sender in sender_vocab_freq) {
      print_key_value_pair (sender, calc_ratio (sender_vocab_freq[sender][word] ?? 0, text_freq[sender], 4));
    }
  }
}

function matrix_metric_words_uniq_to_user (sender_vocab_uniq, sender_vocab_freq) {
  console.log (`\nTop ~${default_word_list_size} words unique to user`);
  for (const sender in sender_vocab_uniq) {
    console.log (`  ${sender}:`);

    let uniqs = sender_vocab_uniq[sender];
    uniqs.sort((a, b) => { return sender_vocab_freq[sender][b] - sender_vocab_freq[sender][a] });

    /* trim down the list to our target size (or a little more if there's a tie) */
    uniqs = trim_list_fuzzy (uniqs, sender_vocab_freq[sender]);

    for (const i in uniqs) {
      const uniq = uniqs[i];
      print_key_value_pair(`  ${parseInt (i) + 1}. ${uniq}`, sender_vocab_freq[sender][uniq], (i >= default_word_list_size ? " (tie)" : ""));
    }
  }
}

function calc_ratio (numerator, denominator, num_places = 2) {
  const mod = Math.pow (10, num_places);
  return Math.round (numerator * mod / denominator) / mod;
}

function print_key_value_pair (key, value, note = "") {
  if (key.match (/@[a-zA-Z0-9]+:matrix.org/)) {
    key = key.replace ("@", "").replace (":matrix.org", "");
  }

  let line = `  ${key}:`;
  for (let i = line.length; i < 40; i++) {
    line += " ";
  }
  for (let i = `${value}`.length; i < 10; i++) {
    line += " ";
  }
  line += `${value}`;

  line += ` ${note}`;

  console.log (line);
}

/* Print frequency table, but sorted by value, from least to most frequently occurring */
function freq_show_vsort (freq, threshold = 0, trim = false) {
  let keys = Object.keys (freq).filter (key => freq[key] >= threshold);

  keys.sort ((a,b) => (freq[b] ?? 0) - (freq[a] ?? 0));

  if (trim) {
    keys = trim_list_fuzzy (keys, freq);
  }

  for (let i in keys) {
    const key = keys[i];
    print_key_value_pair ((parseInt (i)+1)+`. ${key}`, freq[key] ?? 0, (trim && i >= default_word_list_size ? " (tie)" : ""));
  }
}

/* Print frequency table, sorted alphabetically by key */
function freq_show (freq) {
  let keys = Object.keys (freq);
  keys.sort ((a,b) => a < b ? -1 : 1 );
  for (const key of keys) {
    print_key_value_pair (key, freq[key] ?? 0);
  }
}

function trim_list_fuzzy (keys, freq) {
  if (keys.length > default_word_list_size) {
    let threshold_i = default_word_list_size;
    let threshold_freq = freq[keys[threshold_i-1]];
    keys = keys.filter(word => freq[word] >= threshold_freq );
    if (keys.length > extended_word_list_size) {
      const num_hidden = keys.length - extended_word_list_size;
      keys = keys.slice (0, extended_word_list_size);
      if (num_hidden > 0) {
        console.log (`  (there are an additional ${num_hidden} words with freq ${threshold_freq} not shown)`);
      }
    }
  }

  return keys;
}

function normalize (word) {
  /* TODO: consider stripping out ', >, ", etc. */
  return word.toLowerCase ().replace (/[~{}?!"'.()*/,><\\\-:_’“#=&$+[\]@^`]/g, "");
}

function history_metrics (err, json_data_str) {
  if (err) {
    console.error (err.message);
    process.exit (1);
  }

  const history = JSON.parse (json_data_str);
  console.log ("Chat name: ", history.room_name);

  const first_dt = new Date (history.messages[0].origin_server_ts);
  const last_dt = new Date (history.messages.at (-1).origin_server_ts);
  const num_days = (last_dt.getTime () - first_dt.getTime ()) / 1000 / 60 / 60 / 24;

  console.log (`\nChat Duration:
  First Message: ${first_dt}
  Last Message:  ${last_dt}
  Num Days:      ${num_days}`);

  const sender_freq = {};
  const msgtype_freq = {};
  const image_freq = {};
  const video_freq = {};
  const text_freq = {};
  const text_word_freq = {};
  const text_char_freq = {};
  const text_iword_freq = {};
  const all_word_freq = {};
  const all_vocab_set = new Set ();
  const sender_vocab_set = {};
  const sender_vocab_size = {};
  const sender_vocab_freq = {};
  const sender_vocab_uniq = {};

  for (const msg of history.messages) {
    sender_freq[msg.sender] = (sender_freq[msg.sender] ?? 0) + 1;

    if (msg.content) {
      if (msg.content.msgtype) {
        msgtype_freq[msg.content.msgtype] = (msgtype_freq[msg.content.msgtype] ?? 0) + 1;
        if (msg.content.msgtype === "m.video") {
          video_freq[msg.sender] = (video_freq[msg.sender] ?? 0) + 1;
        }
        if (msg.content.msgtype === "m.image") {
          image_freq[msg.sender] = (image_freq[msg.sender] ?? 0) + 1;
        }
        if (msg.content.msgtype === "m.text") {
          text_freq[msg.sender] = (text_freq[msg.sender] ?? 0) + 1;

          if (msg.content.body) {
            const words = msg.content.body.split (/[ \n\t]/);
            text_word_freq[msg.sender] = (text_word_freq[msg.sender] ?? 0) + words.length;

            for (const word_raw of words) {
              const word = normalize (word_raw);

              if (word.length == 0) {
                continue;
              }

              text_char_freq[msg.sender] = (text_char_freq[msg.sender] ?? 0) + word.length;

              if (!sender_vocab_set[msg.sender]) {
                sender_vocab_set[msg.sender] = new Set ();
              }
              all_vocab_set.add (word);
              if (!sender_vocab_set[msg.sender].has (word)) {
                sender_vocab_set[msg.sender].add (word);
                sender_vocab_size[msg.sender] = (sender_vocab_size[msg.sender] ?? 0) + 1;
              }
              if (!sender_vocab_freq[msg.sender]) {
                sender_vocab_freq[msg.sender] = {};
              }
              sender_vocab_freq[msg.sender][word] = (sender_vocab_freq[msg.sender][word] ?? 0) + 1;

              if (words_of_interest.has(word)) {
                text_iword_freq[msg.sender] = (text_iword_freq[msg.sender] ?? 0) + 1;
              }

              if (skip_stop_words === false || !stop_words_set.has (word)) {
                all_word_freq[word] = (all_word_freq[word] ?? 0) + 1;
              }
            }
          }
        }
      }
    }
  }

  for (const word of all_vocab_set) {
    const sender_with_word = [];
    for (const sender in sender_vocab_set) {
      if (sender_vocab_set[sender].has (word)) {
        sender_with_word.push (sender);
      }
    }
    if (sender_with_word.length == 1) {
      const sender = sender_with_word[0];
      if (!sender_vocab_uniq[sender]) {
        sender_vocab_uniq[sender] = [];
      }
      sender_vocab_uniq[sender].push (word); /* TODO add a count too */
    }
  }

  console.log ("\n# of messages sent by all users: ", history.messages.length);

  console.log ("\n# of each message type:");
  freq_show (msgtype_freq);

  console.log ("\n# of messages sent by user:");
  freq_show (sender_freq);

  console.log ("\n# of images sent by user:");
  freq_show (image_freq);

  console.log ("\n# of videos sent by user:");
  freq_show (video_freq);

  console.log ("\n# of text messages sent by user:");
  freq_show (text_freq);

  console.log ("\n# of words used by user:");
  freq_show (text_word_freq);

  console.log ("\n# of characters typed by user:");
  freq_show (text_char_freq);

  console.log ("\naverage words per message by user:");
  for (const sender in text_word_freq) {
    print_key_value_pair (sender, calc_ratio (text_word_freq[sender], text_freq[sender]));
  }

  console.log ("\naverage word length by user");
  for (const sender in text_word_freq) {
    print_key_value_pair (sender, calc_ratio (text_char_freq[sender], text_word_freq[sender]));
  }

  console.log ("\nvocab size by user:");
  freq_show (sender_vocab_size);

  console.log ("\n# words per day by user");
  for (const sender in text_word_freq) {
    print_key_value_pair (sender, calc_ratio (text_word_freq [sender], num_days));
  }

  console.log ("\n# messages per day by user (inc. text, image, video, etc.)");
  for (const sender in text_word_freq) {
    print_key_value_pair (sender, calc_ratio (sender_freq [sender], num_days));
  }

  if (words_of_interest.size > 0) {
    matrix_metric_words_of_interest (text_freq, text_iword_freq, sender_vocab_freq);
  } else {
    console.log ("\nNOTE: not showing any 'words of interest' stats; use -i, --words-of-interest-file, or --words-of-interest to show.");
  }

  if (show_all_word_freq) {
    console.log (`\nGeneral frequency of words (by all users; top ~${default_word_list_size}; skip_stop_words? ${skip_stop_words}):`);
    freq_show_vsort (all_word_freq, all_word_freq_threshold, true);
  } else {
    console.log ("\nNOTE: not showing general word frequency; use -g or --show-general-word-freq to show");
  }

  if (show_words_uniq_to_user) {
    matrix_metric_words_uniq_to_user (sender_vocab_uniq, sender_vocab_freq);
  } else {
    console.log ("\nNOTE: not showing words unique to users.  Use -u or --show-words-uniq-to-user to show");
  }
}

function arg_check_missing_value (args, i_next, option, option_type_desc) {
  if (i_next >= args.length) {
    console.error (`ERROR: option '${option}' specified, but it requires ${option_type_desc} and none given`);
    process.exit (1);
  }
}

function load_word_file (fpath) {
  let list = [];
  try {
    list = fs.readFileSync (fpath).toString ().split ("\n").filter (word => word !== "");
  } catch (e) {
    // console.error (`${e.message}.  Skipping file.`);
  }
  return list;
}

function print_help () {
  console.log (`Usage: ${app_name} [options] <file>

Generate stats/metrics on a Matrix JSON chat log file.

General Word Frequency Options:
  These options are for general word frequency metrics *not* specific to individual
  users.

  -g, --show-general-word-freq     Show a metric for word frequency over all users.
  --general-word-freq-threshold N  Only show words with freq of N or greater (default: ${all_word_freq_threshold}).
  --include-stop-words             Don't ignore 'stop words' (overly common words) for general word frequency.
                                   By default, they're omitted.
  --stop-words-file PATH           Specify a alternative file of 'stop words' (words to skip)
                                   (Default at '${stop_words_path}')

Options:

  -u, --show-words-uniq-to-user    Show tables of words unique to each user.
  -n N, --word-list-size N         Target number of words to report for general word frequency and words unique
                                   to user frequency.  (Default is ${default_word_list_size} but will show up
                                   to ${extended_word_list_size} if there is a tie.)
  --words-of-interest WORDS        Specify a comma-delimited list of words of interest.
  -i, --words-of-interest-file PATH
                                   Specify a file of words of interest to see metrics for each word.
                                   (Default at '${words_of_interest_path}').
  -h, --help                       Show this help text.

Project: https://gitlab.com/aquarichy/matrix-metric
`);
}

const app_name = path.basename (process.argv[1]);

let skip_stop_words = true;
let show_all_word_freq = false;
let show_words_uniq_to_user = false;
let all_word_freq_threshold = 5;
let default_word_list_size = 5;
let extended_word_list_size = 10;
let words_of_interest_path = path.join ("/", os.homedir (), ".config/matrix-metric/words_of_interest.txt");
let stop_words_path = path.join ("/", os.homedir (), ".local/share/matrix-metric/stop_words.txt"); /* default "stop_words.txt" derived from https://gist.github.com/sebleier/554280 */
let words_of_interest = new Set ();

let process_options = true;
const files = [];
const args = process.argv.slice (2);

if (args.length == 0) {
  print_help ();
  process.exit (1);
}

for (let i = 0; i < args.length; i++) {
  const arg = args[i];

  if (process_options) {
    switch (arg) {
      case "-g":
      case "--show-general-word-freq":
        show_all_word_freq = true;
        continue;
      case "--general-word-freq-threshold":
        arg_check_missing_value(args, i++, arg, "an integer");
        all_word_freq_threshold = args[i];
        continue;
      case "--include-stop-words":
        skip_stop_words = false;
        continue;
      case "--stop-words-file":
        arg_check_missing_value(args, i++, arg, "a file path");
        stop_words_path = args[i];
        fs.access (stop_words_path, (err) => {
          if (err) {
            console.error (err.message);
            process.exit (1);
          }
        });
        continue;
      case "-u":
      case "--show-words-uniq-to-user":
        show_words_uniq_to_user = true;
        continue;
      case "-n":
      case "--word-list-size":
        arg_check_missing_value(args, i++, arg, "an integer");
        default_word_list_size = parseInt (args[i]);
        extended_word_list_size = parseInt (default_word_list_size) + 5;
        continue;
      case "-i":
      case "--words-of-interest-file":
        arg_check_missing_value(args, i++, arg, "a file path");
        words_of_interest_path = args[i];
        fs.access (words_of_interest_path, (err) => {
          if (err) {
            console.error (err.message);
            process.exit (1);
          }
        });
        continue;
      case "--words-of-interest":
        arg_check_missing_value (args, i++, arg, "string of comma-separated words");
        args[i].split (",").forEach (word => words_of_interest.add (word));
        continue;
      case "-h":
      case "--help":
        print_help ();
        process.exit (0);
      case "--":
        process_options = false;
        continue;
      default:
        if (arg.length > 0 && arg[0] == "-") {
          console.error(`ERROR: Unrecognized option, '${arg}'`);
          process.exit(1);
        }
    }
  }

  files.push (arg);
}

load_word_file (words_of_interest_path).forEach (word => words_of_interest.add (word));
const stop_words_set = new Set (load_word_file (stop_words_path));

if (files.length == 0) {
  console.error (`Usage: ${app_name} [options] <file>`);
  process.exit (1);
} else if (files.length > 1) {
  console.log (files);
  console.log ("Sorry, we only support one file at a time right now.");
  process.exit (1);
}

/* TODO: take filename as a CLI argument */
fs.readFile (files[0], "utf8", history_metrics);
