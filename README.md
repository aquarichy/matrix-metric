# matrix-metric

Generates a bunch of fun metrics by parsing JSON chat history from a Matrix chat.
Written in JavaScript for use with Nodejs.

Some example metrics: 

- \# of messages sent by user
- \# of each message type
- \# of images sent by user
- \# of videos sent by user
- \# of text messages sent by user
- \# of words used by user
- \# of characters typed by user
- \# words per day by user
- \# messages per day by user (inc. text, image, video, etc.)

## Usage:

I used it as a script, running under Nodejs.  Right now, it expects
the chat history JSON file to be called `matrix.json` and exist in your
current working directory.  Eventually, we'll add some command-line argument processing.

`$ node matrix-metric.mjs`

## Example Output

``` shell
Bass Battlers

Chat Duration:
  First Message: Sat Aug 01 2022 03:50:38 GMT-0800
  Last Message:  Thu Dec 02 2023 00:55:18 GMT-0900
  Num Days:      487.9199159837963

# of messages sent by user:
  ramona:                                     6090
  scott:                                      6492
  todd:                                       2529

# of each message type:
  m.audio:                                      22
  m.bad.encrypted:                              13
  m.file:                                        1
  m.image:                                    1172
  m.text:                                    13707
  m.video:                                      26

# of images sent by user:
  ramona:                                      789
  scott:                                       257
  todd:                                        126

# of videos sent by user:
  ramona:                                        7
  scott:                                         8
  todd:                                         11

# of text messages sent by user:
  ramona:                                     5212
  scott:                                      6116
  todd:                                       2379

# of words used by user:
  ramona:                                   164366
  scott:                                    151030
  todd:                                      51087

# of characters typed by user:
  ramona:                                   718127
  scott:                                    631694
  todd:                                     210084

average words per message by user:
  ramona:                                    31.54
  scott:                                     24.69
  todd:                                      21.47

average word length by user
  ramona:                                     4.37
  scott:                                      4.18
  todd:                                       4.11

vocab size by user:
  ramona:                                    15473
  scott:                                     12131
  todd:                                       6611

# words per day by user
  ramona:                                   336.87
  scott:                                    309.54
  todd:                                      104.7

# messages per day by user (inc. text, image, video, etc.)
  ramona:                                    12.48
  scott:                                     13.31
  todd:                                       5.18

Frequency of 'vegan' by user:
  ramona:                                      152
  scott:                                       140
  todd:                                         83

Probability that a message contains 'vegan' by user (1.0 == 100%, 0.01 == 1%):
  ramona:                                   0.0292
  scott:                                    0.0229
  todd:                                     0.0349
```

## License

GPLv3+

## Authors

Richard Schwarting <aquarichy@gmail.com> © 2023
