PREFIX=${HOME}/.local

check: .eslint-passed

.eslint-passed: bin/matrix-metric.mjs
	npx eslint $^
	touch $@

install:
	install bin/matrix-metric.mjs ${PREFIX}/bin/
	test -d ${PREFIX}/share/matrix-metric || mkdir ${PREFIX}/share/matrix-metric
	cp share/matrix-metric/stop_words.txt ${PREFIX}/share/matrix-metric/

uninstall:
	rm -f ${PREFIX}/bin/matrix-metric.mjs
	rm -f ${PREFIX}/share/matrix-metric/stop_words.txt

clean:
	rm -f .eslint-passed
	find . -type f -name "*~" -delete -or -type f -name "#*#" -delete
